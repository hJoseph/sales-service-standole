package com.fullstack.sales.service01.salesservice.service;

import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.domain.Gender;
import com.fullstack.sales.service01.salesservice.model.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class ClientReadByGenderAndEmailService {

    private Gender  gender;

    private String email;

    private Client client;

    @Autowired
    ClientRepository clientRepository;


    public void execute(){
        client = findClientByEmailAndState(email,gender);
    }

    private Client findClientByEmailAndState(String email, Gender gender) {
        return clientRepository.findClientByEmailAndGender(email,gender)
                .orElse(null);
        //return  null;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
