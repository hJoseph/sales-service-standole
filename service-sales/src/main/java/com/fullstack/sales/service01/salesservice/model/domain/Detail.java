package com.fullstack.sales.service01.salesservice.model.domain;

import lombok.Data;
import javax.persistence.*;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
@Entity
@Table(name = "Detail_table")
//@DiscriminatorColumn(name = "tipo")
public class Detail {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "totalProducts", length = 100, nullable = false)
    private Integer totalProducts;

    @Column(name = "totalPrice", length = 100, nullable = false)
    private Integer totalPrice;

}
