package com.fullstack.sales.service01.salesservice.model.impl;

import com.fullstack.sales.service01.salesservice.model.domain.Person;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class PersonUserBuilder {

    private AccountPersonImpl instance;

    public static PersonUserBuilder getInstance(Person person) {
        return (new PersonUserBuilder()).setPerson(person);
    }

    private PersonUserBuilder() {
        instance = new AccountPersonImpl();
    }

    private PersonUserBuilder setPerson(Person person) {
        instance.setPersonId(person.getId());
        return this;
    }

    public AccountPersonImpl build() {
        return instance;
    }
}
