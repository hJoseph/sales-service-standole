package com.fullstack.sales.service01.salesservice.service;

import com.fullstack.sales.service01.salesservice.input.ClientInput;
import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class ClientCreateService {
    private ClientInput clientInput;

    @Autowired
    private ClientRepository clientRepository;

    public Client save(){
        return clientRepository.save(composeClientInstance());
    }

    private Client composeClientInstance() {
        Client instance = new Client();
        instance.setLastPurchase(clientInput.getLastPurchase());
        instance.setFirstName(clientInput.getFirstName());
        instance.setLastName(clientInput.getLastName());
        instance.setEmail(clientInput.getEmail());
        instance.setIsDeleted(clientInput.getDeleted());
        instance.setGender(clientInput.getGender());
        return  instance;
    }

    public ClientInput getClientInput() {
        return clientInput;
    }

    public void setClientInput(ClientInput clientInput) {
        this.clientInput = clientInput;
    }


}
