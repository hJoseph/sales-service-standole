package com.fullstack.sales.service01.salesservice.command;

import com.fullstack.sales.service01.salesservice.input.ClientInput;
import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.repositories.ClientRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Autor Henry Joseph Calani A.
 **/
@SynchronousExecution
public class ClientCreateCmd  implements BusinessLogicCommand {

    private ClientInput clientInput;

    @Autowired
    private ClientRepository clientRepository;

    @Getter
    private Client client;

    @Override
    public void execute() {
        clientRepository.save(composeClientInstance());
    }

    private Client composeClientInstance() {
        Client instance = new Client();
        instance.setLastPurchase(clientInput.getLastPurchase());
        instance.setFirstName(clientInput.getFirstName());
        instance.setLastName(clientInput.getLastName());
        instance.setEmail(clientInput.getEmail());
        instance.setIsDeleted(clientInput.getDeleted());
        instance.setGender(clientInput.getGender());
        return  instance;
    }

    public ClientInput getClientInput() {
        return clientInput;
    }

    public void setClientInput(ClientInput clientInput) {
        this.clientInput = clientInput;
    }
}
