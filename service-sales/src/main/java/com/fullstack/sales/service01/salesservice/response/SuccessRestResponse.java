package com.fullstack.sales.service01.salesservice.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class SuccessRestResponse {

    private static final String TYPE = "SuccessResponse";

    private static final String DEFAULT_CONTENT = "SUCCESS";

    public SuccessRestResponse() {

        //super(TYPE);
    }

    @JsonProperty("data")
    public String getContent() {
        return DEFAULT_CONTENT;
    }

}
