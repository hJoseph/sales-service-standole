package com.fullstack.sales.service01.salesservice.model.impl;

import com.dh.fullstack.users.api.model.Gender;
import com.dh.fullstack.users.api.model.system.RootUser;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class RootPersonImpl implements RootUser {


    private Long  personId;
    private String  email;
    private String  firsName;
    private String  lastName;
    private Boolean  isDeleted;
    private Gender genderType;

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirsName(String firsName) {
        this.firsName = firsName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public void setGenderType(Gender genderType) {
        this.genderType = genderType;
    }


    @Override
    public Long getPersonId() {
        return null;
    }

    @Override
    public String getFirstNameId() {
        return null;
    }

    @Override
    public String getLastNameId() {
        return null;
    }

    @Override
    public Boolean getIdDelete() {
        return null;
    }

    @Override
    public Gender getAccountStatus() {
        return null;
    }
}
