package com.fullstack.sales.service01.salesservice;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Santiago Mamani
 */
@Configuration
@EnableFeignClients
@ComponentScan("com.fullstack.sales.service01.salesservice")
public class Config {
}
