package com.fullstack.sales.service01.salesservice.cliente.service;

import com.dh.chat.contact.api.input.SystemContactCreateInput;

import com.fullstack.sales.service01.salesservice.cliente.model.Contact;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Autor Henry Joseph Calani A.
 */
@FeignClient("${contact.service.name}")
interface ModuleContactClient {

    @RequestMapping(
            value = "/system/contacts",
            method = RequestMethod.POST
    )
    Contact createContact(@RequestBody SystemContactCreateInput input);
}
