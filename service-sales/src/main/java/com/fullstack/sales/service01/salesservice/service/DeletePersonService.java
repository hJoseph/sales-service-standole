package com.fullstack.sales.service01.salesservice.service;

import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.domain.Gender;
import com.fullstack.sales.service01.salesservice.model.domain.Person;
import com.fullstack.sales.service01.salesservice.model.repositories.ClientRepository;
import com.fullstack.sales.service01.salesservice.model.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class DeletePersonService {


    private Long idPerson;

    private Person person;

    @Autowired
    PersonRepository personRepository;


    public void execute(){
        deletePerson(idPerson);
    }

    public void deletePerson(Long idPerson) {
        if(personRepository.existsById(idPerson)) {

            Optional<Person> personInformation = findPersonlById(idPerson);
            personInformation.get().setIsDeleted(Boolean.TRUE);
            personRepository.save(personInformation.get());
        }
        else
        {
            System.out.println("No exist the Person with id '" + idPerson + "' for delete.");
        }
    }

    public Optional<Person> findPersonlById(Long idPerson) {

        return personRepository.findById(idPerson);
    }

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
