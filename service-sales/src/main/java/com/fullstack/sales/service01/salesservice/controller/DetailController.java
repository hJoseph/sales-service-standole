package com.fullstack.sales.service01.salesservice.controller;

import com.fullstack.sales.service01.salesservice.input.DetailInput;
import com.fullstack.sales.service01.salesservice.model.domain.Detail;
import com.fullstack.sales.service01.salesservice.service.DetailCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Api(
        tags = "Detail rest",
        description = "Operations over Detail"
)
@RestController
@RequestMapping("/Detail")
@RequestScope
public class DetailController {

    @Autowired
    private DetailCreateService detailCreateService;

    @ApiOperation(
            value = "Endpoint to create Detail"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create client"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Not fount test"
            )
    })

    @RequestMapping(method = RequestMethod.POST)
    public Detail createDetail(@RequestBody DetailInput input) {
        detailCreateService.setDetailInput(input);
        return detailCreateService.save();
    }




}
