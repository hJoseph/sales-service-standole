package com.fullstack.sales.service01.salesservice.controller.api;

import com.fullstack.sales.service01.salesservice.command.ClientCreateCmd;
import com.fullstack.sales.service01.salesservice.controller.Constants;
import com.fullstack.sales.service01.salesservice.input.ClientInput;
//import com.sales.api.model.Client;
import com.fullstack.sales.service01.salesservice.model.domain.Client;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/

@Api(
        tags = Constants.ContactTag.NAME,
        description = Constants.ContactTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.SECURE_CONTACTS)
@RequestScope
public class ClientCreateController {

   @Autowired
    ClientCreateCmd clientCreateCmd;

    @ApiOperation(
            value = "Create a client single"
    )

    @RequestMapping(method = RequestMethod.POST)
    public Client createClient(@RequestBody ClientInput input) {
        clientCreateCmd.setClientInput(input);
        clientCreateCmd.execute();
        return clientCreateCmd.getClient();

    }
}
