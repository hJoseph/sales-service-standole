package com.fullstack.sales.service01.salesservice.model.impl;

import com.dh.fullstack.users.api.model.Gender;
import com.dh.fullstack.users.api.model.system.AccountPerson;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class AccountPersonImpl implements AccountPerson {

    private Long  personId;
    private Long  email;
    private Long  firsName;
    private Long  lastName;
    private Long  isDeleted;
    private Gender genderType;

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public void setEmail(Long email) {
        this.email = email;
    }

    public void setFirsName(Long firsName) {
        this.firsName = firsName;
    }

    public void setLastName(Long lastName) {
        this.lastName = lastName;
    }

    public void setIsDeleted(Long isDeleted) {
        this.isDeleted = isDeleted;
    }

    public void setGenderType(Gender genderType) {
        this.genderType = genderType;
    }


    @Override
    public Long getPersonId() {
        return null;
    }

    @Override
    public String getEmail() {
        return null;
    }

    @Override
    public String getFirsName() {
        return null;
    }

    @Override
    public String getLastName() {
        return null;
    }

    @Override
    public Boolean getIsDeleted() {
        return null;
    }

    @Override
    public Gender getGenderType() {
        return null;
    }
}
