package com.fullstack.sales.service01.salesservice.cliente.service;

import com.dh.chat.contact.api.input.SystemContactCreateInput;
import com.fullstack.sales.service01.salesservice.cliente.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 */
@Service
public class SystemContactService {

    @Autowired
    private ModuleContactClient client;

    public Contact createContact(SystemContactCreateInput input) {
        return client.createContact(input);
    }
}
