package com.fullstack.sales.service01.salesservice.cliente.model;

import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 */
public class Contact implements com.dh.chat.contact.api.model.Contact {

    private Long contactId;

    private Long userId;

    private Long accountId;

    private String email;

    private String name;

    private String avatarId;

    private Date createdDate;

    @Override
    public Long getContactId() {
        return contactId;
    }

    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public Long getAccountId() {
        return accountId;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAvatarId() {
        return avatarId;
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }
}
