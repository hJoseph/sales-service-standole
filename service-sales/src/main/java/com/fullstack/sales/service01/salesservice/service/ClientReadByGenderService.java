package com.fullstack.sales.service01.salesservice.service;

import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.domain.Gender;
import com.fullstack.sales.service01.salesservice.model.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class ClientReadByGenderService {

    private Gender  gender;

    private List<Client>  clientList;

    @Autowired
    private ClientRepository clientRepository;

    public void execute(){
        clientList = findClientByGender(gender);
    }

    private  List<Client> findClientByGender(Gender gender){

        return clientRepository.findAllByGender(gender);
    }
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<Client> getClientList() {
        return clientList;
    }

    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }
}
