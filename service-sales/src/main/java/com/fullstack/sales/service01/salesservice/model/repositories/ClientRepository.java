package com.fullstack.sales.service01.salesservice.model.repositories;

import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.domain.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface ClientRepository extends JpaRepository<Client, Long> {

    List<Client> findAllByGender(Gender gender);

    @Query("select item from Client item where item.email = :email and item.gender = :gender")
    Optional<Client> findClientByEmailAndGender(@Param("email") String email, @Param("gender") Gender gender);




}
