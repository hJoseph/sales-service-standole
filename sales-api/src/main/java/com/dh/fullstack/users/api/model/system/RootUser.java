package com.dh.fullstack.users.api.model.system;

import com.dh.fullstack.users.api.model.Gender;

import java.io.Serializable;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface RootUser extends Serializable {

    Long getPersonId();

    String getFirstNameId();

    String getLastNameId();

    Boolean getIdDelete();

    Gender getAccountStatus();
}