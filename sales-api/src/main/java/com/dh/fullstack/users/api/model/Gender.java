package com.dh.fullstack.users.api.model;

/**
 * @Autor Henry Joseph Calani A.
 **/
public enum Gender {
    MALE,
    FEMALE
}
